angular.module('table.controllers', [])

.service('UserService', function($http) {

  var baseurl = 'https://obscure-ocean-2327.herokuapp.com';

  this.getRestaurants = function(position, callback) {

    var get_restaurants = baseurl + "/restaurants?lat=" + position.lat + "&lng=" + position.lng;

    $http.get(get_restaurants).
    success(function(data, status, headers, config) {
      callback(data);
    }).
    error(function(data, status, headers, config) {
      var error = data;
    });
  }

  this.getReservations = function(userId, callback) {

    var url = baseurl + "/user/" + userId;

    $http.get(url).
    success(function(data, status, headers, config) {
      callback(data);
    }).
    error(function(data, status, headers, config) {
      var error = data;
    });
  }

  this.closeReservation = function(resId, callback) {

    var url = baseurl + "/reservations/" + resId + "/close";

    $http.post(url).
    success(function(data, status, headers, config) {
      callback(data);
    }).
    error(function(data, status, headers, config) {
      var error = data;
    });

  }

  this.addReservation = function(payload, callback) {

    var url = baseurl + "/reservations";

    $http.post(url, payload).
    success(function(data, status, headers, config) {
      callback(data);
    }).
    error(function(data, status, headers, config) {
      var error = data;
    });

  }

  this.acceptTable = function(resId, callback) {

    var url = baseurl + "/reservations/" + resId + "/accept";

    $http.post(url).
    success(function(data, status, headers, config) {
      callback(data);
    }).
    error(function(data, status, headers, config) {
      var error = data;
    });

  }

})

.controller('AppCtrl', function($scope, $ionicModal, $timeout, $location) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {
    name: 'kevin j',
    phone: '5132935023'
  };

  console.log('Initilization');

  $scope.position = {
    lat: 0,
    lng: 0
  };

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.loginModal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.loginModal.hide();
    $location.path("/tab/list");
  };

  // Open the login modal
  $scope.login = function() {
    $scope.loginModal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    navigator.geolocation.getCurrentPosition(function(pos) {
      $scope.position = {
        lat: pos.coords.latitude,
        lng: pos.coords.longitude
      };

      console.log('Current location', $scope.position);
      // $scope.loading.hide();

    }, function(error) {
      alert('Unable to get location: ' + error.message);
    });

    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };

})


.controller('ListController', ['$scope', '$http', '$state',
  '$ionicListDelegate', '$ionicLoading', '$timeout', '$ionicModal', 'UserService',
  function($scope, $http, $state, $ionicListDelegate,
    $ionicLoading, $timeout, $ionicModal, UserService) {

    $scope.notifications = [];
    $scope.places = [];
    var currentReservations = [];
    $scope.whichplace = $state.params.pid;

    var payload = {
      customerName: $scope.loginData.name,
      userId: $scope.loginData.phone,
      partySize: '4',
      restaurantId: ''
    };

    $scope.data = {
      showReorder: false,
      filter: false
    }

    $ionicModal.fromTemplateUrl('templates/notifications.html', {
      scope: $scope
    }).then(function(modal) {
      $scope.notificationsModal = modal;
    });
    
    var checkForSeatings = function(reservations) {
      var notifications = [];

      reservations.forEach(function(reservation) {
        if( reservation.seatingNow && !reservation.closed){
          notifications.push(reservation);
        }
      });

      if( notifications.length > 0) {
        $scope.notifications = notifications.slice();
        $scope.notificationsModal.show();
      } else {
        $scope.notificationsModal.hide();
      }
    };

    $ionicLoading.show({
      duration: 30000,
      noBackdrop: true,
      template: '<p class="item-icon-left">finding your table<ion-spinner icon="lines"/></p>'
    });

    $scope.removeItem = function(item) {
      $scope.places.splice($scope.places.indexOf(item), 1);
    };

    $scope.toggleStar = function(item) {
      item.star = !item.star;
      if (item.star) {
        $scope.addReservation(item);
      } else {
        $scope.closeReservation(item);
      }

      $ionicListDelegate.closeOptionButtons();
    };

    $scope.moveItem = function(item, fromIndex, toIndex) {
      $scope.places.splice(fromIndex, 1);
      $scope.places.splice(toIndex, 0, item);
    };

    $scope.doRefresh = function() {
      UserService.getRestaurants($scope.position, function(data) {
        $scope.places = data;
        $scope.$broadcast('scroll.refreshComplete');
      });
    };

    $scope.addReservation = function(item) {
      payload.restaurantId = item.id;
      payload.waitTime = item.waitTime;
      console.log(payload);
      UserService.addReservation(payload, function(data) {});
    };

    $scope.closeReservation = function(item) {

      currentReservations.forEach(function(reservation) {
        if (reservation.restaurantId == item.id) {
          UserService.closeReservation(reservation.reservationId, function(data) {
            $scope.doRefresh(); 
          });
          item.star = false;
        }

      });
    };

    $scope.acceptTable = function(reservationId) {

          UserService.acceptTable(reservationId, function(data) {});
    };

    $scope.declineTable = function(reservationId) {

          UserService.closeReservation(reservationId, function(data) {});

    };

    $scope.getReservations = function(userId) {
      UserService.getReservations(userId, function(data) {
        currentReservations = data;
        if ($scope.places.length > 0) {
          mungeData($scope.places, currentReservations);
        }

      });
    };

    $scope.pollReservations = function(userId) {
      $scope.getReservations(userId);
      checkForSeatings(currentReservations);
    };

    var mungeData = function(places, reservations) {
      places.forEach(function(place) {
        var aReservation = {};
        place.star = reservations.some(function(reservation) {
          if(reservation.restaurantId == place.id) {
            aReservation = reservation;
            return true;
          }
        });
        if(place.star) {
          var timeStamp = parseInt(aReservation.tStamp);
          var millisDiff = new Date().getTime() - timeStamp;
          var minutesDiff = millisDiff / 60000; //it's magic!       
          var remaining = Math.ceil(aReservation.waitTime - minutesDiff);
          var message = remaining + "m remaining";
          place.waitTime = message; 
        }
      });
    };

    var poll = function() {
      $timeout(function() {
        $scope.pollReservations(payload.userId);
        poll();
      }, 2000);
    };

    poll();

    UserService.getRestaurants($scope.position, function(data) {
      $scope.places = data;
      if (currentReservations.length > 0) {
        mungeData($scope.places, currentReservations);
      }
      $ionicLoading.hide();
    });
  }
]);
